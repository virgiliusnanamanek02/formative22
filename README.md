# Formative module 22

## Resource:

1.[ Design and Prototype](https://www.figma.com/file/8AiXPYJvnVBdq1l06m80c8/cooking-recipe?type=design&node-id=0:1&mode=dev&t=KSyf0PWrT5sq3dBc-1)
2. [Mockup Preview](https://www.figma.com/proto/8AiXPYJvnVBdq1l06m80c8/cooking-recipe?type=design&node-id=21-1483&t=oEUV2YFWr3mCJ6v2-1&scaling=scale-down&page-id=0:1&mode=design)

## Schema:


### Tabel `Recipe`
- Tabel utama yang menyimpan informasi dasar tentang setiap resep.

| Kolom          | Tipe Data      | Deskripsi                              |
| -------------- | -------------- | -------------------------------------- |
| recipe_id      | INT            | Primary key, ID unik untuk setiap resep |
| title          | VARCHAR(255)   | Nama resep                             |
| photo_url      | VARCHAR(255)   | URL gambar resep                       |
| duration       | INT            | Durasi memasak dalam menit             |
| is_iked          | boolean            | Apakah disukai atau tidak                            |

### Tabel `RecipeTags`
- Tabel yang menyimpan informasi tentang tag atau kategori dari setiap resep.

| Kolom          | Tipe Data      | Deskripsi                              |
| -------------- | -------------- | -------------------------------------- |
| tag_id         | INT            | Primary key, ID unik untuk setiap tag  |
| recipe_id      | INT            | Foreign key, ID resep terkait          |
| tag_name       | VARCHAR(50)    | Nama tag atau kategori resep           |

### Tabel `Ingredients`
- Tabel yang menyimpan bahan-bahan dari setiap resep.

| Kolom          | Tipe Data      | Deskripsi                              |
| -------------- | -------------- | -------------------------------------- |
| ingredient_id  | INT            | Primary key, ID unik untuk setiap bahan|
| recipe_id      | INT            | Foreign key, ID resep terkait          |
| ingredient_name| VARCHAR(255)   | Nama bahan                             |
| quantity       | VARCHAR(50)    | Jumlah atau takaran bahan              |
| unit           | VARCHAR(50)    | Satuan takaran bahan                   |

### Tabel `Steps`
- Tabel yang menyimpan langkah-langkah memasak dari setiap resep.

| Kolom          | Tipe Data      | Deskripsi                              |
| -------------- | -------------- | -------------------------------------- |
| step_id        | INT            | Primary key, ID unik untuk setiap langkah|
| recipe_id      | INT            | Foreign key, ID resep terkait          |
| step_number    | INT            | Nomor langkah                          |
| description    | TEXT           | Deskripsi langkah memasak              |

### Tabel `FavoriteRecipes`
- Tabel yang menyimpan resep yang disukai oleh pengguna.

| Kolom          | Tipe Data      | Deskripsi                              |
| -------------- | -------------- | -------------------------------------- |
| favorite_id    | INT            | Primary key, ID unik untuk setiap resep yang disukai|
| user_id        | INT            | Foreign key, ID pengguna yang menyimpan resep|
| recipe_id      | INT            | Foreign key, ID resep yang disukai     |

### Tabel `SavedRecipes`
- Tabel yang menyimpan resep yang disimpan oleh pengguna.

| Kolom          | Tipe Data      | Deskripsi                              |
| -------------- | -------------- | -------------------------------------- |
| saved_id       | INT            | Primary key, ID unik untuk setiap resep yang disimpan|
| user_id        | INT            | Foreign key, ID pengguna yang menyimpan resep|
| recipe_id      | INT            | Foreign key, ID resep yang disimpan    |
